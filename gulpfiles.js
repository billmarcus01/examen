var gulp = require('gulp');
var sourcemaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var cssmin = require('gulp-cssmin');




gulp.task('style', function () {
    gulp.src('src/**/*.js')
    gulp.src('src/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer())
        .pipe(cssmin())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('.tmp'))
        .pipe(gulp.dest('dist'))

})

gulp.task('default', ['style', 'html']);